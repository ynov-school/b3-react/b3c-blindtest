import { Center, Text } from "@chakra-ui/react";
import React from "react";

interface Props {}

export const E404: React.FC<Props> = () => {
return (
  <Center>
    <Text fontSize="6xl">404</Text>
  </Center>
) 
}