import React from "react";
import { QueryCache, QueryClient } from "react-query";
import { ButtonLarge } from "../components/atoms/ButtonLarge";
import { QuestionLayout } from "../components/organisms/QuestionLayout";
import { useQuestions } from "../hooks/useQuestions";

interface Props {

}

export const Game: React.FC<Props> = () => {

  const { data, status, error, isFetching, refetch} = useQuestions()

  console.log(data);
  
  const test = () => {
    refetch()
  }
  
return (
  
  <div>
    {status === "success" ? (
        <QuestionLayout question={data?.questions[0]}/>
      ) : null
    }
  <div>
    <ButtonLarge text="Start !" action={test}/>
  </div>
  </div>
  ) 
}