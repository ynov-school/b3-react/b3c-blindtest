import React from "react";
import { Box, Center, Flex, Spacer, StackDivider, Text, VStack } from "@chakra-ui/react";
import { useProfil } from "../hooks/useProfil";
import { ButtonLarge } from "../components/atoms/ButtonLarge";
import { useHistory } from "react-router-dom";
import { useQuestions } from "../hooks/useQuestions";


interface Props {}

export const Home: React.FC<Props> = () => {
  const { data, status } = useProfil()
  const history = useHistory();

  const goHomePage = () => {
    history.push('/app/game')
    refetch()
  }

  const {refetch} = useQuestions()
 
    
  return (
    <Flex direction="column" alignItems="center" justify="space-around" >
      <Center>
        <VStack
        divider={<StackDivider borderColor="gray.200" />}
        spacing={4}
        align="stretch"
        >
        <Box>
          <Text align="center" fontSize="6xl">
              Hello {data?.player.name}
          </Text>
        </Box>
        <Box>
            {status === 'loading' && (
              <div>Loading data...</div>
            )}

            {status === 'error' && (
              <div>Error fetching data...</div>
            )}

            {status === 'success' && (
              <Text align="center">Vous avez joué <strong> {data?.player.nb_played_games} </strong>  parties</Text>
            )}
        </Box>
        </VStack>
       
      </Center>
      <Box>
          <ButtonLarge text="Start game" action={() => goHomePage()} />
      </Box>
    </Flex>

  ) 
}