import { Box } from "@chakra-ui/react";
import React from "react";
import { useHistory } from "react-router-dom";
import { FormLogIn } from "../components/organisms/FormLogIn";
import { useUserStore } from "../store";

interface Props {}

export const LogIn: React.FC<Props> = () => {
  const user = useUserStore(({user}) => user)
  const history = useHistory();
  if (user) history.push('/app')

  return (
    <Box borderWidth="1px" h="fit-content" p="10" w="100%" borderRadius="lg" overflow="hidden" >
      <FormLogIn />
    </Box>
  ) 
}