import { Avatar, Box, StackDivider, VStack, Text } from "@chakra-ui/react";
import React from "react";
import { useProfil } from "../hooks/useProfil";

interface Props {

}

export const Profil: React.FC<Props> = () => {

  const { data, status } = useProfil()
  
return (
  <VStack
    divider={<StackDivider borderColor="gray.200" />}
    spacing={4}
    align="stretch"
    >
    <Box>
      <Avatar size="2xl" name="Segun Adebayo" src={data?.player?.avatar} />{" "}
    </Box>
    <Box>
      <Text align="center" fontSize="6xl">
        {data?.player.name}
      </Text>
    </Box>
  </VStack>
) 
}