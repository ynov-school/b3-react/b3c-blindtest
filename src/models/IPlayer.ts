export interface IPlayer {
  status: string
  player: IData
}

interface IData {
  id: string
  name: string
  update_date: number
  nb_played_games: number
  avatar: string
  backoffice: boolean
}