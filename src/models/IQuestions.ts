export interface IQuestionsData {
  status: string
  questions: IQuestion[]
}

export interface IQuestion {
  answers: Tuple<string, 4>
  id: string
  audio_url: string
  good_answer: string
  creator: string
  type: string
  question: string
}

type Tuple<TItem, TLength extends number> = [TItem, ...TItem[]] & { length: TLength };
