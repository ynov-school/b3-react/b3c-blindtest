import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import App from './App';
import firebase from 'firebase/app'
import 'firebase/firestore'
import 'firebase/auth'
import { ChakraProvider, ColorModeScript, extendTheme, ThemeConfig } from '@chakra-ui/react';
import { firebaseConfig } from './config/firebase';

const config: ThemeConfig = {
  initialColorMode: "light",
  useSystemColorMode: false,
  
}
const theme = extendTheme({ config })


firebase.initializeApp(firebaseConfig)


ReactDOM.render(
  <React.StrictMode>
    <ChakraProvider theme={theme}>
    <ColorModeScript initialColorMode={theme.config.initialColorMode} />
    <div className="container">
      <App/>
    </div>
    </ChakraProvider>
  </React.StrictMode>,
  document.getElementById('root')
);

