import React from 'react';
import './App.css';

import { BrowserRouter, Redirect, Route, Switch } from 'react-router-dom';
import { Header } from './components/organisms/Header';
import { Home } from './pages/Home';
import { E404 } from './pages/404';
import { LogIn } from './pages/LogIn';
import { useUserStore } from './store';
import { QueryClient, QueryClientProvider } from 'react-query';
import { Profil } from './pages/Profil';
import { Game } from './pages/Game';



const App: React.FC = () => {
  const qc = new QueryClient({
    defaultOptions: {
      queries: {
        refetchOnWindowFocus: false,
      },
    },
  });

  return (
    <QueryClientProvider client={qc}>
      <BrowserRouter>
        <Header/>
        <div className="main">
          <Switch>
            <Route path="/" exact component={LogIn} />
            <Route path="/login" exact component={LogIn} />
            <PrivateRoute path="/app" component={Home} exact/>
            <PrivateRoute path="/app/profil" component={Profil} exact/>
            <PrivateRoute path="/app/game" component={Game} exact/>
            <Route path="/"component={E404} />
          </Switch>
        </div>  
      </BrowserRouter> 
    </QueryClientProvider>
    
  )
}

const PrivateRoute: React.FC<{
  component: React.FC;
  path: string;
  exact: boolean;
}> = (props) => {
  const user = useUserStore(({user}) => user)

  return user ? (
    <Route 
      path={props.path} 
      exact={props.exact}
      component={props.component}
    />) : (
    <Redirect to="/"/>)
};
export default App