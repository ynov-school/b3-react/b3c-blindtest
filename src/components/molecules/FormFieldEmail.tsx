import { FormControl, FormLabel, Input } from "@chakra-ui/react";
import React from "react";

const fit = {
  width: 'auto'
}

interface Props {
  field: string
  value: string
}

export const FormFieldEmail: React.FC<Props> = ({field, value}: Props) => {
  return (
    <FormControl id="email" m="5" isRequired style={fit} >
      <FormLabel>Email address</FormLabel>
      <Input 
        {...field}
        type="email"
        placeholder="bob@gmail.com"
        value={value}
        autoComplete="email"
      />
    </FormControl>
  ) 
}