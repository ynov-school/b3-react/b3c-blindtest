import { Box } from "@chakra-ui/react";
import React from "react";
import { useHistory } from "react-router-dom";
import { useUserStore } from "../../store";
import { ButtonLarge } from "../atoms/ButtonLarge";
import { ButtonTheme } from "../atoms/ButtonTheme";
import { MenuHamburger } from "../atoms/MenuHamburger";


interface Props {}

export const Navbar: React.FC<Props> = () => {
  const user = useUserStore(({user}) => user)
  const history = useHistory();

  const toLogInPage = () => {
  history.push('login')
  }

  return (
    <Box>
      <ButtonTheme />
      <MenuHamburger />
      { !user ? (
        <ButtonLarge 
          text="Log In"
          action={() => toLogInPage()} />) 
        : undefined
      }
    </Box>
  ) 
}