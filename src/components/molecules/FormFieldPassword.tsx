import { FormControl, FormLabel, Input } from "@chakra-ui/react";
import React from "react";

const fit = {
  width: 'auto'
}

interface Props {
  field: string
  value: string
}

export const FormFieldPassword: React.FC<Props> = ({field, value}: Props) => {
return (
  <FormControl id="password" m="5" isRequired style={fit} >
    <FormLabel>Password</FormLabel>
    <Input 
      {...field}
      type="password"
      value={value}
      autoComplete="current-password"
    />
  </FormControl>
) 
}