import React from "react";
import { IQuestion } from "../../models/IQuestions";
import { Box, Center, SimpleGrid, StackDivider, Text, VStack } from "@chakra-ui/react";
import { Answer } from "./Answer";
import { sumbitAnswer } from "../../apiHelper";


interface Props {
  question: IQuestion | undefined
}

export class UserChoice {
  questionId!: string
  choice!: any
  time!: number

  constructor( questionId: string, choice: any, time: number) {
    this.questionId = questionId
    this.choice = choice
    this.time = time
  }
}

export const QuestionLayout: React.FC<Props> = ({ question }: Props ) => {
  const checkIfCorrect = async (i: any) => {
    const time = 500
    const answer = new UserChoice(question!.id, question?.answers[i], time)
    const test = []
    test.push(answer)
    test.push(answer)
    console.log( await sumbitAnswer(test))
  }
  
  return (
    <VStack
      divider={<StackDivider borderColor="gray.200" />}
      spacing={4}
      align="center"
    >
      <VStack align="center">
        <Text align="center" fontSize="6xl">{question?.question}</Text>
        <audio controls>
          <source src={question?.audio_url} />
        </audio>
      </VStack>
      <SimpleGrid columns={2} spacing={10}>
        {question?.answers.map((answer, i) =>
          <Answer answer={answer} i={i + 1} key={i} onClick={() => checkIfCorrect(i)} />
        )}
      </SimpleGrid>
  </VStack>
  ) 
}