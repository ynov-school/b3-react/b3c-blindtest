import { Box, Flex, Heading, Spacer } from "@chakra-ui/react";
import React from "react";
import { Navbar } from "../molecules/Navbar";

interface Props {}

export const Header: React.FC<Props> = () => {
return (
  <Flex>
    <Box p="2">
      <Heading size="lg">B3C Blindtest</Heading>
    </Box>
    <Spacer />
    <Box>
      <Navbar />
    </Box>
  </Flex>
) 
}