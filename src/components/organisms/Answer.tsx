import { Box, Heading, Image, LinkBox, LinkOverlay } from "@chakra-ui/react";
import React from "react";

interface Props {
  answer: string
  i: number
  onClick : (str: number) => void
}

export const Answer: React.FC<Props> = ({answer, i, onClick} : Props) => {

  const expression = /https?:\/\/(www\.)?[-a-zA-Z0-9@:%._\\+~#=]{1,256}\.[a-zA-Z0-9()]{1,6}\b([-a-zA-Z0-9()@:%_\\+.~#?&/\\/=]*)/
  let regex = new RegExp(expression)

return (
  <LinkBox as="button" maxW="sm" p="5" borderWidth="1px" rounded="md" onClick={() => onClick(i)} >
  <Box as="time" dateTime="2021-01-15 15:30:00 +0000 UTC">
    {i}
  </Box>
  <Heading size="md" my="2">
    <LinkOverlay textAlign="center" href="#">
      {answer.match(regex) ? ( <Image src={answer} alt="Segun Adebayo" />) :  answer }
    </LinkOverlay>
  </Heading>
</LinkBox>
) 
}