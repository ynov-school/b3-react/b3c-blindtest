import { Button, Center } from "@chakra-ui/react";
import firebase from "firebase";
import { Field, Form, Formik, FormikProps } from "formik";
import React from "react";
import { FormFieldEmail } from "../molecules/FormFieldEmail";
import { FormFieldPassword } from "../molecules/FormFieldPassword";
import { useUserStore } from "../../store";
import { useHistory } from "react-router-dom";

interface IForm {
  email: string,
  password: string
}

interface Props {}

export const FormLogIn: React.FC<Props> = () => {
  const setUser = useUserStore(({ setUser }) => setUser)
  const history = useHistory();
  const fa = firebase.auth();

  const auth = async (email: string, password: string) => {
    try {
      const userCredential = await fa.signInWithEmailAndPassword(email, password)
      setUser(userCredential.user)
      history.push('/app')
    } catch (error) {
      console.log("tata", error);
    }
  }

  const initialValues: IForm = {
    email : "",
    password : ""
  }

  return (
    <Formik
    initialValues={(initialValues)}
    onSubmit={(values: IForm, actions) => {
      auth(values.email, values.password)
      actions.setSubmitting(false)
    }}>

      {(props: FormikProps<IForm>) =>  (
        <Form>
          <Field name="email">
            {({ field }: {field : string}) => (
              <FormFieldEmail field={field} value={props.values.email} />
            )}
          </Field>
          <Field name="password">
            {({ field }: {field : string}) => (
              <FormFieldPassword field={field} value={props.values.password} />
            )}
          </Field>
          <Center>
            <Button 
              m="5"
              size="lg"
              colorScheme="teal"
              isLoading={props.isSubmitting}
              type="submit">
                Log In
            </Button>
          </Center>
        </Form>
      )}
    </Formik>
  ) 
} 