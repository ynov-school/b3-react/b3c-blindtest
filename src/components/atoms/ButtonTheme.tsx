import { IconButton, useColorMode } from "@chakra-ui/react";
import { MoonIcon, SunIcon } from '@chakra-ui/icons'
import React from "react";

interface Props {}

export const ButtonTheme: React.FC<Props> = () => {
  const { colorMode, toggleColorMode } = useColorMode()

return (
  <IconButton 
    aria-label="theming" 
    size="lg"
    m="1"
    icon={colorMode === "light" ? <MoonIcon /> : <SunIcon />}
    onClick={toggleColorMode}>
     {colorMode === "light" ? "Dark" : "Light"}
  </IconButton>
  ) 
}