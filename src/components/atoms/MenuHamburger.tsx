import React from "react";
import { HamburgerIcon } from "@chakra-ui/icons";
import { Menu, MenuButton, MenuDivider, MenuItem, MenuList, MenuGroup, IconButton } from "@chakra-ui/react";
import { useHistory } from "react-router-dom";
import firebase from "firebase";
import { useUserStore } from "../../store";

interface Props {}

export const MenuHamburger: React.FC<Props> = () => {
  const setUser = useUserStore(({ setUser }) => setUser)
  const history = useHistory();
  
  const toHomePage = () => {
    history.push('/app')
  }

  const toProfilPage = () => {
    history.push('/app/profil')
  }

  const logout = () => {
    firebase.auth().signOut()
    setUser(null)
    history.push('/')
  }

  return (
    <Menu>
      <MenuButton
      as={IconButton}
      aria-label="Options"
      icon={<HamburgerIcon />}
      size="lg"
      m="1"
      variant="outline"
      />
      <MenuList>
        <MenuGroup title="App">
          <MenuItem>Game</MenuItem>
          <MenuItem onClick={toHomePage}>Home</MenuItem>
        </MenuGroup>
        <MenuDivider />
        <MenuGroup title="Settings">
          <MenuItem onClick={toProfilPage}>Profil</MenuItem>
          <MenuItem onClick={logout}>Disconnect</MenuItem>
        </MenuGroup>
      </MenuList>
    </Menu>
  ) 
}