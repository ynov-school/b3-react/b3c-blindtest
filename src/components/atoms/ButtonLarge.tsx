import { Button } from "@chakra-ui/react";
import React from "react";

interface Props {
  text: string
  action: () => void
}

export const ButtonLarge: React.FC<Props> = ({text, action}: Props) => {
return (
  <Button colorScheme="teal" size="lg" m="1" onClick={action}>
    {text}
  </Button>
) 
}