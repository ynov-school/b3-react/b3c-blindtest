import { QueryObserverResult, useQuery } from "react-query"
import firebase from "firebase";
import { IPlayer } from "../models/IPlayer";

const getProfil = async (id: string) => {
  const res = await fetch (`${process.env.REACT_APP_API_URL}players?id=${id}`)
  return res.json()
}

export const useProfil = () : QueryObserverResult<IPlayer> => {
  const fa = firebase.auth();
  const userId = fa.currentUser?.uid as string
  return useQuery(["profil", userId], () => getProfil(userId), {
    enabled: !!userId
  })
}