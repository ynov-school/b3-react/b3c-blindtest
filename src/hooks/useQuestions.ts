import { QueryObserverResult, useQuery } from "react-query"
import firebase from "firebase";
import { IQuestionsData } from "../models/IQuestions";

const getQuestions = async () => {
  const token = await firebase.auth().currentUser?.getIdToken();
  if (token) {
    const res = await fetch (`${process.env.REACT_APP_API_URL}questions`, {
      headers: {
        BlindTestToken: token,
      }
    })
    return res.json()
  }
}

export const useQuestions = () : QueryObserverResult<IQuestionsData> => {
  return useQuery("questions", getQuestions , {
    enabled: false
  })
}