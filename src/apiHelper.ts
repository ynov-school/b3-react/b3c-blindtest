import firebase from "firebase";

export const sumbitAnswer = async (answers: any) => {
  const token = await firebase.auth().currentUser?.getIdToken();
  if (token) {
    const res = await fetch (`${process.env.REACT_APP_API_URL}game`, {
      method: "POST",
      headers: {
        BlindTestToken: token,
        "Content-Type": "application/json",
      },
      body: JSON.stringify(answers)
    })
    return res.json()
  }
}
