import create from 'zustand'
import firebase from "firebase";


export type UserState = {
  user: firebase.User | null
  setUser: (user: firebase.User | null) => void
}

export const useUserStore = create<UserState>((set) => ({
  user: null,
  setUser: (user: firebase.User | null) =>
    set((state) => ({
      ...state,
      user,
    })),
}))

export type ProfilState = {
  profil: firebase.User | null
  setProfil: (user: firebase.User | null) => void
}

export const useProfilStore = create

